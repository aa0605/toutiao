<?php

namespace xing\toutiao;

class TouTiaoService
{

    public $drive;


    /**
     * @param $driveName
     * @return \xing\toutiao\drive\YiiDrive
     */
    public static function getInstance($driveName)
    {
        $drives = [
            'Yii' => '\xing\toutiao\drive\YiiDrive'
        ];
        return new $drives[$driveName];
    }

    public function checkImageRight($url)
    {
        return static::getInstance($this->drive)->checkImageRight($url);
    }
}